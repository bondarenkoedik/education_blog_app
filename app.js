"use strict"

let express        = require("express"),
    mongoose       = require("mongoose"),
    _              = require("underscore"),
    bodyParser     = require("body-parser"),
    flash          = require("express-flash"),
    sanitizeHtml   = require("sanitize-html"),
    methodOverride = require("method-override");

let app = express();

module.exports = app;

// config app

app.set("view engine", "ejs");
app.use('/static', express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride("_method"));

// mongodb config
mongoose.connect("mongodb://localhost/blog_app");

let BlogSchema = mongoose.Schema({
  title: String,
  image: String,
  body: String,
  created: {type: Date, default: Date.now }
});

let Blog = mongoose.model("Blog", BlogSchema);

// middlewares

// sanitize html if req.bddy is specified
app.use((req, res, next) => {
  if (!_.isEmpty(req.body)) {
    req.body.blog = _.mapObject(req.body.blog, v => sanitizeHtml(v));
  }
  next();
});

// routes

app.get("/", (req, res) => {
  res.redirect("/blogs");
});


app.get("/blogs", (req, res) => {
  
  Blog.find({}, (error, blogs) => {
    if (error) {
      console.log(error);
    } else {
      res.render("index", { blogs });
    }
  });

});


app.get("/blogs/new", (req, res) => {
  res.render("new");
});


app.post("/blogs", (req, res) => {

  Blog.create(req.body.blog, (error, post) => {
    if (error) {
      console.log(error);
      res.render("new");
    } else {
      res.redirect(`/blogs/${post._id}`);
    }
  });

});


app.get("/blogs/:id", (req, res) => {

  Blog.findById(req.params.id, (error, post) => {
    if (error) {
      res.redirect("/blogs");
    } else {
      res.render("show", { post });
    }
  });

});


app.get("/blogs/:id/edit", (req, res) => {

  Blog.findById(req.params.id, (error, post) => {
    if (error) {
      console.log(error);
    } else {
      res.render("edit", { post });
    }
  });

});


app.put("/blogs/:id", (req, res) => {

  let newPost = req.body.blog;

  Blog.findById(req.params.id, (error, post) => {

    post.save(function(err) {
      if (err) {
        console.log(err);
      }
    });
  });

  Blog.findByIdAndUpdate(req.params.id, req.body.blog, (err, updatedPost) => {
    if (err) {
      console.log(err);
    } else {
      res.redirect(`/blogs/${req.params.id}`);
    }
  });

});


app.delete("/blogs/:id", (req, res) => {

  Blog.findByIdAndRemove(req.params.id, (error) => {
    if (error) {
      console.log(error);
    } else {
      res.redirect("/blogs");
    }
  });

});

app.listen("3000", () => console.log("Server is running..."));